#ifndef FPMETHOD_H
#define FPMETHOD_H

#include <string>

#include <GraphMol/RDKitBase.h>
#include <DataStructs/ExplicitBitVect.h>

class FingerprintMethod
{
public:
    std::string methodName;
    virtual ExplicitBitVect* CalculateStructureFingerprint(
            const RDKit::ROMol* molecule) const = 0;
};
#endif
