#ifndef SIMBBLANQUET_H
#define SIMBBLANQUET_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityBraunBlanquet: public SimilarityMethod
{
public:
    SimilarityBraunBlanquet();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
