#ifndef SIMTANIMOTO_H
#define SIMTANIMOTO_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityTanimoto : public SimilarityMethod
{
public:
    SimilarityTanimoto();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
