#ifndef SIMTVERSKY_H
#define SIMTVERSKY_H

#include <string>
#include <cstdint.hpp>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityTversky : public SimilarityMethod
{
private:
    double alpha;
    double beta;
public:
    SimilarityTversky(double alpha, double beta);
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
