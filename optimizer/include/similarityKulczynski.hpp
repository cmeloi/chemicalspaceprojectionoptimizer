#ifndef SIMKULCZYNSKI_H
#define SIMKULCZYNSKI_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityKulczynski: public SimilarityMethod
{
public:
    SimilarityKulczynski();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
