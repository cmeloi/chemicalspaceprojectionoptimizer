#ifndef SIMASYMMETRIC_H
#define SIMASYMMETRIC_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityAsymmetric: public SimilarityMethod
{
public:
    SimilarityAsymmetric();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
