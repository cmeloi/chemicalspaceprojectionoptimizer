#ifndef FPLAYERED_H
#define FPLAYERED_H

#include <DataStructs/ExplicitBitVect.h>
#include "fingerprintMethod.hpp"

class FingerprintLayered : public FingerprintMethod {
    public:
        FingerprintLayered();
        ExplicitBitVect* CalculateStructureFingerprint(
                const RDKit::ROMol* molecule) const;
};

#endif
