#ifndef FPMORGAN_H
#define FPMORGAN_H

#include <DataStructs/ExplicitBitVect.h>
#include "fingerprintMethod.hpp"

class FingerprintMorgan : public FingerprintMethod {
    private:
        unsigned int radius;
        unsigned int bits;
    public:
        FingerprintMorgan(int radius);
        ExplicitBitVect* CalculateStructureFingerprint(
                const RDKit::ROMol* molecule) const;
};

#endif
