#ifndef CLUSTER_H
#define CLUSTER_H

#include <GraphMol/RDKitBase.h>
#include <DataStructs/ExplicitBitVect.h>

#include "fingerprintMethod.hpp"
#include "similarityMethod.hpp"
#include "clusterProjection.hpp"

class Cluster
{
private:
    std::vector<RDKit::ROMol*> structures;
    std::vector<FingerprintMethod*> fpMethods;
    std::vector<SimilarityMethod*> scMethods;
    std::vector<std::vector<ExplicitBitVect*> > fingerprints;
    std::vector<ClusterProjection*> projections;
    std::vector<std::vector<ExplicitBitVect*> > CalculateAllFPrints(
            const std::vector<RDKit::ROMol*> molecules, 
            const std::vector<FingerprintMethod*> fpMethods) const;
    std::vector<ExplicitBitVect*> CalculateMethodFPrints(
            const std::vector<RDKit::ROMol*> molecules,
            const FingerprintMethod* fpMethod) const;
    ExplicitBitVect* CalculateStructureFPrint(
            const RDKit::ROMol* molecule, 
            const FingerprintMethod* fpMethod) const;
    std::vector<ClusterProjection*> CalculateAllChemspaceProjections(
            const std::vector<SimilarityMethod*> scMethods, 
            const std::vector<FingerprintMethod*> fpMethods, 
            const std::vector<std::vector<ExplicitBitVect*> > fingerprints) const;
public:
    std::string clusterName;
    float clusterWeight;
    Cluster(float weight = 1.0);
    void AddMoleculeBySmiles(const std::string smiles);
    void RunForFpSc(
            const std::vector<FingerprintMethod*> fpMethods,
            const std::vector<SimilarityMethod*> scMethods);
    double MeanMutualSimilarityOnFpSc(
            const FingerprintMethod* fpMethod, 
            const SimilarityMethod* scMethod) const;    
    double WeightedMutualSimilarityOnFpSc(
            const FingerprintMethod* fpMethod, 
            const SimilarityMethod* scMethod) const;
};
#endif
