#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H

#include "cluster.hpp"
#include "fingerprintMethod.hpp"
#include "similarityMethod.hpp"

class Optimization
{
private:
    std::vector<Cluster> clusters;
    std::vector<FingerprintMethod*> fpMethods;
    std::vector<SimilarityMethod*> scMethods;
    std::string output;
    void CreateClusterIfNoneExist();
public:
    Optimization();
    void ProcessInputLine(std::string inputLine);
    void RunOptimization();
    std::string GetOutput() {return output;}
};
#endif
