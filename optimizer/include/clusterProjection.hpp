#ifndef CLUSTER_PROJECTION_H
#define CLUSTER_PROJECTION_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include "similarityMethod.hpp"
#include "fingerprintMethod.hpp"

class ClusterProjection
{
private:
    const SimilarityMethod* scMethod;
    const FingerprintMethod* fpMethod;
    std::vector<std::vector<double> > projectionMatrix;
    std::vector<std::vector<double> > ComputeProjectionMatrix(
            const SimilarityMethod* scMethod,            
            const std::vector<ExplicitBitVect*> fpsByFpMethod) const;
    double ComputeMeanMutualSimilarity(
            const std::vector<std::vector<double> > projectionMatrix) const;
public:
    std::string projectionName;
    double meanMutualSimilarity;
    ClusterProjection(
            const SimilarityMethod* scMethod, 
            const FingerprintMethod* fpMethod,
            const std::vector<ExplicitBitVect*> fpsByFpMethod);
};
#endif
