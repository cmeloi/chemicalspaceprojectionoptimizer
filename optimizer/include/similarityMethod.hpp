#ifndef SIMMETHOD_H
#define SIMMETHOD_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

class SimilarityMethod
{
public:
    std::string methodName;
    virtual double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1, 
            const ExplicitBitVect* FP2) const = 0;
};
#endif
