#ifndef FPPATTERN_H
#define FPPATTERN_H

#include <DataStructs/ExplicitBitVect.h>
#include "fingerprintMethod.hpp"

class FingerprintPattern : public FingerprintMethod {
    public:
        FingerprintPattern();
        ExplicitBitVect* CalculateStructureFingerprint(
                const RDKit::ROMol* molecule) const;
};

#endif
