#ifndef SIMDICE_H
#define SIMDICE_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityDice: public SimilarityMethod
{
public:
    SimilarityDice();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
