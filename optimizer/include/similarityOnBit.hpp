#ifndef SIMONBIT_H
#define SIMONBIT_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityOnBit: public SimilarityMethod
{
public:
    SimilarityOnBit();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
