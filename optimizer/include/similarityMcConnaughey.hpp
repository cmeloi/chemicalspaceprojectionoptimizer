#ifndef SIMCONNAUGHEY_H
#define SIMCONNAUGHEY_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityMcConnaughey: public SimilarityMethod
{
private:
    double NormalizeSimilarityBetween01(const double rawSimilarity) const;
public:
    SimilarityMcConnaughey();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
