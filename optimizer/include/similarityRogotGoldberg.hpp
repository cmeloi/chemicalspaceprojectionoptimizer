#ifndef SIMRGOLDBERG_H
#define SIMRGOLDBERG_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityRogotGoldberg: public SimilarityMethod
{
public:
    SimilarityRogotGoldberg();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
