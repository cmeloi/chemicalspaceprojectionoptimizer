#ifndef FPRDKF_H
#define FPRDKF_H

#include <DataStructs/ExplicitBitVect.h>
#include "fingerprintMethod.hpp"

class FingerprintRdkf : public FingerprintMethod {
    public:
        FingerprintRdkf();
        ExplicitBitVect* CalculateStructureFingerprint(
                const RDKit::ROMol* molecule) const;
};

#endif
