#ifndef SIMSOKAL_H
#define SIMSOKAL_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilaritySokal: public SimilarityMethod
{
public:
    SimilaritySokal();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
