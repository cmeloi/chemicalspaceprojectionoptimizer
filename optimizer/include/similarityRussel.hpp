#ifndef SIMRUSSEL_H
#define SIMRUSSEL_H

#include <string>
#include <DataStructs/ExplicitBitVect.h>

#include "similarityMethod.hpp"

class SimilarityRussel: public SimilarityMethod
{
public:
    SimilarityRussel();
    double CalculateFingerprintsSimilarity(
            const ExplicitBitVect* FP1,
            const ExplicitBitVect* FP2) const;
};
#endif
