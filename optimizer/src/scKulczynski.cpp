#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityKulczynski.hpp"

SimilarityKulczynski::SimilarityKulczynski() {
    methodName = "Kulczynski";
}

double SimilarityKulczynski::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return KulczynskiSimilarity(*FP1, *FP2);
}
