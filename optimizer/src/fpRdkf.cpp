#include <iostream>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/Fingerprints/Fingerprints.h>
#include <DataStructs/ExplicitBitVect.h>

#include "fingerprintMethod.hpp"
#include "fingerprintRdkf.hpp"

FingerprintRdkf::FingerprintRdkf() {
    methodName = "RDKF";
}

ExplicitBitVect* FingerprintRdkf::CalculateStructureFingerprint(
        const RDKit::ROMol* molecule) const {
    return RDKit::RDKFingerprintMol(*molecule);
}
