#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityTversky.hpp"

SimilarityTversky::SimilarityTversky(double alpha, double beta) {
    methodName = "Tversky";
    this->alpha = alpha;
    this->beta = beta;
}

double SimilarityTversky::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return TverskySimilarity(*FP1, *FP2, alpha, beta);
}
