#include <iostream>
#include <string>
#include <vector>

#include <GraphMol/RDKitBase.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <DataStructs/ExplicitBitVect.h>

#include "cluster.hpp"
#include "fingerprintMethod.hpp"
#include "similarityMethod.hpp"
#include "clusterProjection.hpp"

Cluster::Cluster(float weight) {
    this->clusterWeight = weight;
}

void Cluster::AddMoleculeBySmiles(const std::string smiles) {
    std::cout << "parsing smiles: " << smiles << std::endl;
    RDKit::ROMol* mol = RDKit::SmilesToMol(smiles);
    if (mol) {
        structures.push_back(mol);
    }
}

void Cluster::RunForFpSc(
        const std::vector<FingerprintMethod*> fpMethods, 
        const std::vector<SimilarityMethod*> scMethods) {
    this->fpMethods = fpMethods;
    this->scMethods = scMethods;
    if (structures.size() > 1) {
        std::cout << "running cluster: " << clusterName << std::endl;
        fingerprints = CalculateAllFPrints(structures, fpMethods);
        projections = CalculateAllChemspaceProjections(scMethods, fpMethods, fingerprints);
    }
    else {
        std::cout << "cluster '" << clusterName << "' is not running, less than 2 valid structures found.\n";
    }
}

std::vector<std::vector<ExplicitBitVect*> > Cluster::CalculateAllFPrints(
        const std::vector<RDKit::ROMol*> molecules, 
        const std::vector<FingerprintMethod*> fpMethods) const {
    std::cout << "\tcalculating fingerprints of all valid molecules in cluster\n";
    std::vector<std::vector<ExplicitBitVect*> > structuresMethodsFingerprints;
    for (int i = 0; i < fpMethods.size(); ++i) {
        structuresMethodsFingerprints.push_back(CalculateMethodFPrints(molecules, fpMethods[i]));
    }
    return structuresMethodsFingerprints;
}

std::vector<ExplicitBitVect*> Cluster::CalculateMethodFPrints(
        const std::vector<RDKit::ROMol*> molecules,
        const FingerprintMethod* fpMethod) const {
    std::cout << "\t\tcalculating molecule fingerprints for " << fpMethod->methodName << " method\n";
    std::vector<ExplicitBitVect*> methodFingerprints;
    for (int i = 0; i <molecules.size(); ++i) {
        methodFingerprints.push_back(CalculateStructureFPrint(molecules[i], fpMethod));
    }
    return methodFingerprints;
}

ExplicitBitVect* Cluster::CalculateStructureFPrint(
        const RDKit::ROMol* molecule, 
        const FingerprintMethod* fpMethod) const {
    return fpMethod->CalculateStructureFingerprint(molecule);
}

std::vector<ClusterProjection*> Cluster::CalculateAllChemspaceProjections(
        const std::vector<SimilarityMethod*> scMethods, 
        const std::vector<FingerprintMethod*> fpMethods, 
        const std::vector<std::vector<ExplicitBitVect*> > fingerprints) const {
    std::vector<ClusterProjection*> chemspaceProjections;
    std::cout << "\tcalculating all cluster chemspace projections of " << this->clusterName << std::endl;
    for (int i = 0; i < scMethods.size(); ++i) {
        for (int j = 0; j < fpMethods.size(); ++j) {
            chemspaceProjections.push_back(new ClusterProjection(scMethods[i], fpMethods[j], fingerprints[j]));
        }
    }
    std::cout << "\tcurrent cluster weight is " << this->clusterWeight << std::endl;
    return chemspaceProjections;
}

double Cluster::MeanMutualSimilarityOnFpSc(
        const FingerprintMethod* fpMethod, 
        const SimilarityMethod* scMethod) const {
    int scIdx = -1;
    for (int i = 0; i < scMethods.size(); ++i) {
        if (scMethod == scMethods[i]) {
            scIdx = i;
        }
    }
    int fpIdx = -1;
    for (int i = 0; i < fpMethods.size(); ++i) {
        if (fpMethod == fpMethods[i]) {
            fpIdx = i;
        }
    }
    if (scIdx == -1 or fpIdx == -1) {
        std::cout << "E: projection of FP " << fpMethod->methodName << " and SC " << scMethod->methodName 
            << " is not available in this cluster "<< clusterName <<". Returning 0";
        return 0;
    }
    return projections[scIdx*fpMethods.size() + fpIdx]->meanMutualSimilarity;
}

double Cluster::WeightedMutualSimilarityOnFpSc(
        const FingerprintMethod* fpMethod, 
        const SimilarityMethod* scMethod) const {
    return MeanMutualSimilarityOnFpSc(fpMethod, scMethod) * clusterWeight;
}
