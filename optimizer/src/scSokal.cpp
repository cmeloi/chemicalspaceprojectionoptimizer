#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similaritySokal.hpp"

SimilaritySokal::SimilaritySokal() {
    methodName = "Sokal";
}

double SimilaritySokal::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return SokalSimilarity(*FP1, *FP2);
}
