#include <iostream>
#include <string>
#include <list>
#include <sstream>
#include <vector>
#include <deque>

#include "fingerprintMethod.hpp"
#include "fingerprintMorgan.hpp"
#include "fingerprintRdkf.hpp"
#include "fingerprintLayered.hpp"
#include "fingerprintPattern.hpp"
#include "similarityMethod.hpp"
#include "similarityAsymmetric.hpp"
#include "similarityRussel.hpp"
#include "similarityRogotGoldberg.hpp"
#include "similarityOnBit.hpp"
#include "similarityBraunBlanquet.hpp"
#include "similarityMcConnaughey.hpp"
#include "similarityKulczynski.hpp"
#include "similarityDice.hpp"
#include "similarityTanimoto.hpp"
#include "similarityTversky.hpp"
#include "similaritySokal.hpp"
#include "cluster.hpp"
#include "optimization.hpp"

Optimization::Optimization() {
    fpMethods.push_back(new FingerprintMorgan(3));
    fpMethods.push_back(new FingerprintRdkf());
    fpMethods.push_back(new FingerprintLayered());
    fpMethods.push_back(new FingerprintPattern());
    scMethods.push_back(new SimilarityAsymmetric());
    scMethods.push_back(new SimilarityRussel());
    scMethods.push_back(new SimilarityRogotGoldberg());
    scMethods.push_back(new SimilarityOnBit());
    scMethods.push_back(new SimilarityBraunBlanquet());
    scMethods.push_back(new SimilarityMcConnaughey());
    scMethods.push_back(new SimilarityKulczynski());
    scMethods.push_back(new SimilarityDice());
    scMethods.push_back(new SimilarityTanimoto());
    scMethods.push_back(new SimilaritySokal());
    scMethods.push_back(new SimilarityTversky(0.6, 0.4));
}

void Optimization::ProcessInputLine(std::string inputLine) {
    std::stringstream inputLineStream(inputLine);
    std::string inputStreamBuffer;
    std::vector<std::string> inputTokens;
    while (inputLineStream >> inputStreamBuffer) {
        inputTokens.push_back(inputStreamBuffer);
    }
    if (inputTokens[0] == "cluster") {
        std::cout << "Creating new cluster explicitly\n";
        Cluster cluster;
        clusters.push_back(cluster);
    }
    else if (inputTokens[0] == "name") {
        if (inputTokens.size() < 2) {
            std::cout << "E: no cluster name after the 'name' keyword\n";
        }
        else {
            this->CreateClusterIfNoneExist();
            std::cout << "Naming current cluster as " << inputTokens[1] << std::endl;
            clusters.back().clusterName = inputTokens[1];
        }
    }
    else if (inputTokens[0] == "weight") {
        if (inputTokens.size() < 2) {
            std::cout << "E: no cluster weight float value after the 'weight' keyword\n";
        }
        else {
            double weight = std::atof(inputTokens[1].c_str());
            if (weight == 0.0) {
                std::cout << "W: weight argument '" << inputTokens[1] << "\' parses to float 0.0 !\n\
                    clusters with weight 0.0 have no meaning in optimization!\n";
            }
            clusters.back().clusterWeight = weight;
            std::cout << "cluster weight set to " << weight << std::endl;
        }
    }
    else {
        this->CreateClusterIfNoneExist(); 
        clusters.back().AddMoleculeBySmiles(inputTokens[0]); 
    }
}

void Optimization::RunOptimization() {
    std::cout << "Running optimization" << std::endl;
    for (int i = 0; i < clusters.size(); ++i) {
        clusters[i].RunForFpSc(fpMethods, scMethods);
    }
    double fpScClusterProjectionMatrix [fpMethods.size()][scMethods.size()][clusters.size()];
    double fpScAggregatedMatrix [fpMethods.size()][scMethods.size()];
    double similarity, aggregatedSimilarities;
    for (int fp = 0; fp < fpMethods.size(); ++fp) {
        for (int sc = 0; sc < scMethods.size(); ++sc) {
            aggregatedSimilarities = 0;
            for (int c = 0; c < clusters.size(); ++c) {
                similarity = clusters[c].WeightedMutualSimilarityOnFpSc(
                        fpMethods[fp], scMethods[sc]);
                fpScClusterProjectionMatrix[fp][sc][c] = similarity;
                aggregatedSimilarities += similarity;
            }
            fpScAggregatedMatrix[fp][sc] = aggregatedSimilarities;
        }
    }
    std::cout << "Aggregating cluster data" << std::endl;
    for (int fp = 0; fp < fpMethods.size(); ++fp) {
        for (int sc = 0; sc < scMethods.size(); ++sc) {
            std::cout << fpMethods[fp]->methodName << " | " << scMethods[sc]->methodName << ":\n";
            std::cout << "\t" << fpScAggregatedMatrix[fp][sc] << 
                " weighted cluster similarity differentiation" << std::endl;
            for (int c = 0; c < clusters.size(); ++c) {
                std::cout << "\t\t" << fpScClusterProjectionMatrix[fp][sc][c] << " ~ " <<
                    clusters[c].clusterName << ", @ assigned weight: "<< clusters[c].clusterWeight << std::endl;
            }
        }
    }
    std::cout << "Sorting projections" << std::endl;
    std::deque<std::vector<int> > sortedFpScIdxs;
    double currentSimilarity, comparingSimilarity;
    int currentIndex;
    for (int fp = 0; fp < fpMethods.size(); ++fp) {
        for (int sc = 0; sc < scMethods.size(); ++sc) {
            currentSimilarity = fpScAggregatedMatrix[fp][sc];
            std::vector<int> fpSc;
            fpSc.push_back(fp);
            fpSc.push_back(sc);
            currentIndex = 0;
            for (int i = 0; i < sortedFpScIdxs.size(); ++i) {
                int fpIdx = sortedFpScIdxs[i][0];
                int scIdx = sortedFpScIdxs[i][1];
                comparingSimilarity = fpScAggregatedMatrix[fpIdx][scIdx];
                if (currentSimilarity > comparingSimilarity) {
                    break;
                }
                ++currentIndex;
            }
            std::deque<std::vector<int> >::iterator dit = sortedFpScIdxs.begin()+currentIndex;
            sortedFpScIdxs.insert(dit, fpSc);
        }
    }
    std::cout << "RESULTS: \n\n\n";
    for (int i = 0; i < sortedFpScIdxs.size(); ++i) {
        int fp = sortedFpScIdxs[i][0];
        int sc = sortedFpScIdxs[i][1];
        std::cout << fpMethods[fp]->methodName << " | " << scMethods[sc]->methodName << ":\n";
        std::cout << "\t" << fpScAggregatedMatrix[fp][sc] << 
            " weighted cluster similarity differentiation" << std::endl;
        for (int c = 0; c < clusters.size(); ++c) {
            std::cout << "\t\t" << fpScClusterProjectionMatrix[fp][sc][c] << " ~ " <<
                clusters[c].clusterName << ", @ assigned weight: "<< clusters[c].clusterWeight << std::endl;
        }
    }
}

void Optimization::CreateClusterIfNoneExist() {
    if (clusters.empty()) {
        std::cout << "Creating new cluster implicitly\n";
        Cluster cluster;
        clusters.push_back(cluster);
    }
}
