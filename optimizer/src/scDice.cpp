#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityDice.hpp"

SimilarityDice::SimilarityDice() {
    methodName = "Dice";
}

double SimilarityDice::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return DiceSimilarity(*FP1, *FP2);
}
