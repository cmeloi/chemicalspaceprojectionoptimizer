#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityTanimoto.hpp"

SimilarityTanimoto::SimilarityTanimoto() {
    methodName = "Tanimoto";
}

double SimilarityTanimoto::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return TanimotoSimilarity(*FP1, *FP2);
}
