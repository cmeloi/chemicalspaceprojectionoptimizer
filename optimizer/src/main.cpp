#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>

#include "optimization.hpp"

std::string processInput(std::list<std::string> inputLinesList) {
    Optimization optimization;
    while (!inputLinesList.empty()) {
        optimization.ProcessInputLine(inputLinesList.front());
        inputLinesList.pop_front();
    }
    optimization.RunOptimization();
    return optimization.GetOutput();
}

int main (int argc, char* argv[]) {
    if (argc > 1) {
        char *inputFile = argv[1];
        std::cout << "input file is " << inputFile << std::endl;
        std::ifstream inputStream(inputFile);
        if (!inputStream) {
            std::cout << "can't open file " << inputFile << std::endl;
            return 1;
        }
        std::list<std::string> inputLinesList;
        std::string currentLine;
        while (inputStream) {
            std::getline(inputStream, currentLine);
            if (!currentLine.empty()) {
                inputLinesList.push_back(currentLine);
            }
        }
        std::string output = processInput(inputLinesList);
        std::cout << output << std::endl;
        char *outputFile;
        if (argc == 3) {
           outputFile = argv[2];
           std::cout << "output file is " << outputFile << std::endl;
        }
    }
    else {
        std::cout << "Chemical space projection optimizer" << std::endl
        << "usage: chemspace_projection_optimizer source_file [output_file]" << std::endl
        << "source file contains SMILES of structures, one per line." << std::endl
        << "output contains fingerprint selectors and similarity"
        " measurement methods that provide tightest clusters." << std::endl;
    }
    return 0;
}
