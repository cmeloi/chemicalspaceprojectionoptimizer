#include <iostream>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/Fingerprints/Fingerprints.h>
#include <DataStructs/ExplicitBitVect.h>

#include "fingerprintMethod.hpp"
#include "fingerprintPattern.hpp"

FingerprintPattern::FingerprintPattern() {
    methodName = "Pattern";
}

ExplicitBitVect* FingerprintPattern::CalculateStructureFingerprint(
        const RDKit::ROMol* molecule) const {
    return RDKit::PatternFingerprintMol(*molecule);
}
