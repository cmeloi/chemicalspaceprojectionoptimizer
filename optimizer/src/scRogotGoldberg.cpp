#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityRogotGoldberg.hpp"

SimilarityRogotGoldberg::SimilarityRogotGoldberg() {
    methodName = "RogotGoldberg";
}

double SimilarityRogotGoldberg::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return RogotGoldbergSimilarity(*FP1, *FP2);
}
