#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityMcConnaughey.hpp"

SimilarityMcConnaughey::SimilarityMcConnaughey() {
    methodName = "McConnaughey";
}

double SimilarityMcConnaughey::NormalizeSimilarityBetween01(
        const double rawSimilarity) const {
    return (rawSimilarity + 1) / 2;
}

double SimilarityMcConnaughey::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return NormalizeSimilarityBetween01(McConnaugheySimilarity(*FP1, *FP2));
}
