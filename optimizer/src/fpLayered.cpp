#include <iostream>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/Fingerprints/Fingerprints.h>
#include <DataStructs/ExplicitBitVect.h>

#include "fingerprintMethod.hpp"
#include "fingerprintLayered.hpp"

FingerprintLayered::FingerprintLayered() {
    methodName = "Layered";
}

ExplicitBitVect* FingerprintLayered::CalculateStructureFingerprint(
        const RDKit::ROMol* molecule) const {
    return RDKit::LayeredFingerprintMol(*molecule);
}
