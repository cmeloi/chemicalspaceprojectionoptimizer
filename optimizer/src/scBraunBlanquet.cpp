#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityBraunBlanquet.hpp"

SimilarityBraunBlanquet::SimilarityBraunBlanquet() {
    methodName = "BraunBlanquet";
}

double SimilarityBraunBlanquet::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return BraunBlanquetSimilarity(*FP1, *FP2);
}
