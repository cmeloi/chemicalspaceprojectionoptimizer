#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityAsymmetric.hpp"

SimilarityAsymmetric::SimilarityAsymmetric() {
    methodName = "Asymmetric";
}

double SimilarityAsymmetric::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return AsymmetricSimilarity(*FP1, *FP2);
}
