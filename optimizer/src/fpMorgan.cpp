#include <iostream>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/Fingerprints/MorganFingerprints.h>
#include <DataStructs/ExplicitBitVect.h>

#include "fingerprintMethod.hpp"
#include "fingerprintMorgan.hpp"

FingerprintMorgan::FingerprintMorgan(int radius) {
    methodName = "Morgan";
    bits = 2048;
    this->radius = radius;
}

ExplicitBitVect* FingerprintMorgan::CalculateStructureFingerprint(
        const RDKit::ROMol* molecule) const {
    return RDKit::MorganFingerprints::getFingerprintAsBitVect(*molecule, radius, bits);
}
