#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityOnBit.hpp"

SimilarityOnBit::SimilarityOnBit() {
    methodName = "OnBit";
}

double SimilarityOnBit::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return OnBitSimilarity(*FP1, *FP2);
}
