#include <iostream>
#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include <DataStructs/BitOps.h>

#include "similarityRussel.hpp"

SimilarityRussel::SimilarityRussel() {
    methodName = "Russel";
}

double SimilarityRussel::CalculateFingerprintsSimilarity(
        const ExplicitBitVect* FP1,
        const ExplicitBitVect* FP2) const {
    return RusselSimilarity(*FP1, *FP2);
}
