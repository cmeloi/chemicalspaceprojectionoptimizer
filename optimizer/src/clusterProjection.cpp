#include <string>
#include <DataStructs/ExplicitBitVect.h>
#include "similarityMethod.hpp"
#include "fingerprintMethod.hpp"
#include "clusterProjection.hpp"

ClusterProjection::ClusterProjection(
        const SimilarityMethod* scMethod, 
        const FingerprintMethod* fpMethod,
        const std::vector<ExplicitBitVect*> fpsByFpMethod) {
    projectionName = "FP: " + fpMethod->methodName + " | SC: " + scMethod->methodName; 
    std::cout << "\t\t\tcreating a chemspace projection " << projectionName << std::endl;
    scMethod = scMethod;
    fpMethod = fpMethod;
    projectionMatrix = ComputeProjectionMatrix(scMethod, fpsByFpMethod);
    meanMutualSimilarity = ComputeMeanMutualSimilarity(projectionMatrix);
    std::cout << "\t\t\t\t>> projection mean mutual similarity is " << meanMutualSimilarity << std::endl;
}

std::vector<std::vector<double> > ClusterProjection::ComputeProjectionMatrix(
        const SimilarityMethod* scMethod,
        const std::vector<ExplicitBitVect*> fpsByFpMethod) const {
    std::vector<std::vector<double> > simMatrix;
    int matrixRows = fpsByFpMethod.size() - 1;
    for (int i = 0; i <= matrixRows; ++i) {
        std::vector<double> matrixRow;
        for (int j = i + 1; j <= matrixRows; ++j) {
            matrixRow.push_back(scMethod->CalculateFingerprintsSimilarity(
                        fpsByFpMethod[i], 
                        fpsByFpMethod[j]));
        }
        if (i != matrixRows) {
            simMatrix.push_back(matrixRow);
        }
    }
    return simMatrix;
}    

double ClusterProjection::ComputeMeanMutualSimilarity(
        const std::vector<std::vector<double> > projectionMatrix) const {
    double similaritySum = 0;
    int similaritiesCount = 0;
    for (int i = 0; i < projectionMatrix.size(); ++i) {
        std::cout << "\t\t\t\t";
        for (int j = 0; j < projectionMatrix[i].size(); ++j) {
            similaritySum += projectionMatrix[i][j];
            ++similaritiesCount;
            std::cout << projectionMatrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
    return similaritySum / similaritiesCount;
}
